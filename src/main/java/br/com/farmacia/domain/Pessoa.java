package br.com.farmacia.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Entity
public class Pessoa extends GenericDomain {
	
	@ManyToOne
    @JoinColumn(name = "codigo_cidade", nullable = false)
	private Cidade cidade;
	
	@Column(length = 80, nullable = false)
	private String nome;
	
	@Column(length = 14, nullable = true)
	private String cpf;
	
	@Column(length = 20, nullable = true)
	private String rg;
	
	@Column(length = 80, nullable = true)
	private String rua;
	
	@Column(length = 5, nullable = true)
	private Short numero;
	
	@Column(length = 80, nullable = true)
	private String bairro;
	
	@Column(length = 9, nullable = true)
	private String cep;
	
	@Column(length = 10, nullable = true)
	private String complemento;
	
	@Column(length = 10, nullable = true)
	private String telefone;
	
	@Column(length = 10, nullable = true)
	private String celular;
	
	@Column(length = 80, nullable = true)
	private String email;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public Short getNumero() {
		return numero;
	}
	public void setNumero(Short numero) {
		this.numero = numero;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Cidade getCidade() {
		return cidade;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	
}
