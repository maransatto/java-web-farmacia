package br.com.farmacia.test;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import br.com.farmacia.dao.CidadeDAO;
import br.com.farmacia.dao.EstadoDAO;
import br.com.farmacia.domain.Cidade;
import br.com.farmacia.domain.Estado;

public class CidadeDAOTest {
	
	@Test
	@Ignore
	public void salvar() {
		
		Cidade cidade = new Cidade();
		CidadeDAO cidadeDAO = new CidadeDAO();
		
		Estado estado = new Estado();
		EstadoDAO estadoDAO = new EstadoDAO();
		
		estado = estadoDAO.buscar(1L);
		
		cidade.setNome("Pompéia");
		cidade.setEstado(estado);
		
		cidadeDAO.salvar(cidade);
	}
	
	@Test
	public void listar() {
		
		CidadeDAO cidadeDAO = new CidadeDAO();
		List<Cidade> resultado = cidadeDAO.listar();
		
		System.out.println("Total de Registros encontrados: " + resultado.size());
		
		for (Cidade cidade : resultado) {
			System.out.println(cidade.getNome() + " - " + cidade.getEstado().getNome());
		}
		
	}
	
}
